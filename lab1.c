#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#include <stdio.h>
#include <stdint.h>

/**
 * 
 * A representação dos números pares tem o LSB igual a 0;
 * A representação dos números impares tem o LSB igual a 1;
 * Fazemos uma operação & (end bit a bit) com a máscara para
 * 1, que tem somente o LSB com valor 1. Assim, somente o LSB tem 
 * seu valor preservado, o restante dos bits será anulado e o valor 
 * representado será 0 para valores pares e 1 para valores impares.
 * Usamos o operador ! (not) para inverter esse valor.
 * 
 */
int32_t ehPar(int32_t x){
	return !(1&x);
}

/**
 * 
 * Ao cálcular o mod de 8 os bits que restam na representação binaria são
 * somente os 3 bits menos significativos;
 * O máscara do valor 7 tem os 3 bits menos significativos iguais a 1;
 * Fazendo uma operação & (end bit a bit) com a máscara para
 * 7, serão preservados apenas os últimos 3 bits menos significativos, 
 * o restante dos bits será anulado e o valor. Isso nos do valor do mod de
 * x por 8.
 * 
 */
int32_t mod8(int32_t x){
	return 7&x;
}
/**
 * Para calcular o complemento de dois de um número basta
 * calcular o complemento de um e somar um ao valor;
 * 
 * O bit mais significativo (MSB) é o que informa o sinal do número.
 * Se este dígito for 0 o número é positivo, e se for 1 é negativo;
 * 
 * Números positivos teua magnitude é representada na sua forma binária direta,
 * e um bit de sinal 0 é colocado na frente do MSB;
 * 
 * Negativos número ua magnitude é representada na forma de complemento a 2, e 
 * um bit de sinal 1 é colocado na frente do MSB;
 * 
 * O algorítimo de converção de um número entre negativo e positivo em complemento a 2
 *  consiste em inverter todos os bits na representação binária (transformar zeros 
 * em uns e uns em zeros) e somar 1 ao LSB. 
 * 
 */
int32_t negativo(int32_t x) {
	return (~x)+1;
}

/** 
 * 
 * De acordo como o teorema de De Morgan:
 * 		!A ou !B = !(A e B)
 * Como se trata de representações binárias e queremos comparar
 * bit a bit, usamos ~ (not bitwise) para inverter todos os 
 * valores dos bit. 
 * 
*/
int32_t bitwiseAnd(int32_t x, int32_t y) {
    return ~((~x)|(~y));
}

/**
 * 
 * O operador ^ (xor) retorna 0 para valores iguais, retorna 1
 * para valores diferentes não nulos e retorna o valor não nulo
 * da entrada para valores diferentes com apenas um valor nulo;
 * O operador ! (not) inverte valores nulos em não nulos e valores
 * não nulos em nulos;
 * Exemplo:
 * 		!1 = 0
 * 		!0 = 1
 * 		!4 = 0
 * 
*/
int32_t ehIgual(int32_t x, int32_t y) {
	return !(x^y);
}

/** 
 * 
 * Como a representação binária de 1 só tem O LSB como bit não nulo
 * deslocamos o LSB n bits para a esquerda e usamos ~ para inveter os 
 * valores. Assim o n-esimo bit terá valor 0 e os demais serão 1, que
 * usaremos como máscara;
 * Usamos o operador & para comparar todos os bits, o que fará com que
 * todos, exceto o n-esimo bit, de x serão mantidos.
 * 
*/
int32_t limpaBitN(int32_t x, int8_t n) {
    return (~(1<<n))&x;
}

int32_t setaByteEmP(int32_t x, int32_t y, uint8_t p) {
    return (x & ( ~(0xff<<(p<<3)) ) ) | (y<<(p<<3));
}

int32_t minimo(int32_t x, int32_t y) {
    return ((-(x<y))&x)|((-(x>y))&y);
}

void teste(int32_t saida, int32_t esperado) {
    static uint8_t test_number = 0;
    test_number++;
    if(saida == esperado)
        printf(ANSI_COLOR_GREEN "PASSOU! Saída: %-10d\t Esperado: %-10d\n" ANSI_COLOR_RESET,
            saida, esperado);

    else
        printf(ANSI_COLOR_RED "%d: FALHOU! Saída: %-10d\t Esperado: %-10d\n" ANSI_COLOR_RESET,
            test_number, saida, esperado);
}

int main() {
	puts(ANSI_COLOR_BLUE "Primeiro lab - bits" ANSI_COLOR_RESET);
    puts("");

    puts("Teste: ehPar");
    teste(ehPar(2), 1);
    teste(ehPar(1), 0);
    teste(ehPar(3), 0);
    teste(ehPar(13), 0);
    teste(ehPar(100), 1);
    teste(ehPar(125), 0);
    teste(ehPar(1024), 1);
    teste(ehPar(2019), 0);
    teste(ehPar(2020), 1);
    teste(ehPar(-1), 0);
    teste(ehPar(-27), 0);
    teste(ehPar(-1073741825), 0);
    teste(ehPar(1073741824), 1);
    teste(ehPar(2147483647), 0);
    teste(ehPar(-2147483648), 1);
    teste(ehPar(0), 1);
    puts("");

    puts("Teste: mod8");
    teste(mod8(0), 0);
    teste(mod8(4), 4);
    teste(mod8(7), 7);
    teste(mod8(8), 0);
    teste(mod8(-1), 7);
    teste(mod8(-8), 0);
    teste(mod8(2147483647), 7);
    teste(mod8(-2147483648), 0);
    puts("");

    puts("Teste: negativo");
    teste(negativo(0), 0);
    teste(negativo(1), -1);
    teste(negativo(-1), 1);
    teste(negativo(2147483647), -2147483647);
    teste(negativo(-2147483647), 2147483647);
    teste(negativo(-2147483648), 2147483648);
    puts("");

    puts("Teste: bitwiseAnd");
    teste(bitwiseAnd(1, 3), 1);
    teste(bitwiseAnd(-1, 0), 0);
    teste(bitwiseAnd(-1, 0x7FFFFFFF), 0x7FFFFFFF);
    teste(bitwiseAnd(0b0100, 0b1100), 0b0100);
    puts("");

	puts("Teste: ehIgual");
    teste(ehIgual(1,1), 1);
    teste(ehIgual(1,0), 0);
    teste(ehIgual(0,1), 0);
    teste(ehIgual(-1,1), 0);
    teste(ehIgual(-1,-1), 1);
    teste(ehIgual(2147483647,-1), 0);
    teste(ehIgual(2147483647,-2147483647), 0);
    teste(ehIgual(2147483647,-2147483648), 0);
    teste(ehIgual(2147483647,-2147483648), 0);
    puts("");

    puts("Teste: limpaBitN");
    teste(limpaBitN(1,0), 0);
    teste(limpaBitN(0b1111,1), 0b1101);
    teste(limpaBitN(15,3), 7);
    teste(limpaBitN(-1,31), 2147483647);
    teste(limpaBitN(-1,0), -2);
    teste(limpaBitN(2147483647, 30), 1073741823);
    puts("");

    puts("Teste: setaByteEmP");
    teste(setaByteEmP(0x00, 0xFF, 0), 0x000000FF);
    teste(setaByteEmP(0x00, 0xFF, 1), 0x0000FF00);
    teste(setaByteEmP(0x00, 0xFF, 2), 0x00FF0000);
    teste(setaByteEmP(0x00, 0xFF, 3), 0xFF000000);
    teste(setaByteEmP(0x01234567, 0x33, 2), 0x01334567);
    teste(setaByteEmP(0xdeadbeef, 0x00, 0), 0xdeadbe00);
    teste(setaByteEmP(0xdeadbeef, 0x00, 1), 0xdead00ef);
    puts("");

    puts("Teste: minimo");
    teste(minimo(0,1), 0);
    teste(minimo(0,10), 0);
    teste(minimo(1, 128), 1);
    teste(minimo(-1, 0), -1);
    teste(minimo(-1, -2), -2);
    teste(minimo(2147483647, 2147483646), 2147483646);
    teste(minimo(-2147483648, -2147483647), -2147483648);
    teste(minimo(-2147483648, -1), -2147483648);
    puts("");
	
	return 0;
}